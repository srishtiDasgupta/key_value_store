1. With the help of sockets and associated port number, the multiple clients can connect to the server. Whenever, a new client connects to the network, a separate thread is spawned by the main thread to handle the requests from that particular client. To prevent overwriting of values and race condition, mutex lock has been used so that the critical section, i.e, the part where the clients send data to the server and the server handles the request, is executed atomically.

2. A class named: KV has been defined in the header file, where two values namely key and value have been declared. These two values are always strings. A third value, called "isDirty" which serves as a dirty bit is also defined. This bit is set when the value exists in the cache, but is not found in the HashTable. Another value "usageCount" keeps track of the frequency of access of a particular key-value pair. A vector of KV class objects is defined and this vector serves as the KEY-VALUE store.

3. A cache following LFU has also been defined. The capacity of the cache can be changed in Line 32 in the file: server.cpp. Here, the default size has been given as 1, but this can be changed according to the user requirements. 

4. When the client issues a request to the server, the server "locks" the critical section and handles the request. It first accesses the cache. 
		
		a. If the request is to DELETE a key-value, it checks if the key exists in the cache. If not, it searches the HashTable. Once the value has been retrieved, the server checks if the Dirty Bit is set or not. If the dirty bit is set, then the value needs to be removed only from the cache, else the HashTable is also edited.
		
		b. If the request is to PUT a key-value, the server checks if the key exists, either in the HashTable or in the Cache. If the key does not exist in either, the server then checks if the Cache has reached its full capacity. If so, then the function executing the cache eviction strategy is called and the new key-value pair is inserted or "pushed back" into the cache.
		
		c. If the request is to GET a key-value pair, the server again checks if the value exists either in the cache or in the Hashtable and returns True or False accordingly.
	
5. Whenever the server searches for a key, it first searches in the cache. If the key is not found in the cache, only then does it search in the HashTable. To bring the key from the HashTable to the Cache when the Cache is full, cache eviction functionality is executed.

6. In the cache eviction strategy, the key which is least frequently acessed is determined by comparing the "usageCount" value. The dirty bit of this particular key is then checked. If it is set, the key value pair needs to be evicted only from the cache, else the pair has to be evicted from the HashTable too.

7. The HashTable object created is sent as a parameter during the constructor call of the Cache. In this way, during future works, when persistant storage is implemented, the HashTable values will remian in the memory even if the Cache gets defined every time the program is run. 

8. BUILD AND RUN:
		
		1. Open a terminal window. 
		
		2. Go to the program folder
		
		3. Type: g++ server.cpp HashTable.cpp Cache.cpp -o server -std=c++14
		
		4. Open any number of terminals for client 
		
		5. Type: g++ KV_cli.cpp -o client
		
		6. In the server terminal, type: ./server
		
		7. In the client terminals, type: ./client 32001
		
		8. A menu is printed in all the client terminals
		
		9. You can choose any and press enter

9. USER INPUTS:
		
		1. In the server program, the cache size and the initial size of the hash table can be given as input to their respective constructors. 
		
		2. Based on the user options, confirmations of actions (insertion, eviction and retrieval ) are printed out. 

10. SAMPLE TESTCASE: 
		
		--Run both client and server. Default values of cache size is 1 and that of hash table is 2.
		
		1. insert : KEY: munich, VALUE: germany
		
		2. insert : KEY: delhi ,VALUE: india
		
		3.  insert : KEY: paris ,VALUE: france
		
		4. insert : KEY: vienna ,VALUE: austria
		
		5. delete : KEY: vienna
		
		6. print hashtable
