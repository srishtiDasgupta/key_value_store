#ifndef HASHTABLE_HASHTABLE_H
#define HASHTABLE_HASHTABLE_H
#endif

#include <string>
#include <vector>

class HashTable 
{
public:
    HashTable(int n);
    ~HashTable();
    int maxBucket;
    bool enter(std::string key, std::string value);
    std::string retrieve(std::string key);
    bool remove(std::string key);
    void printTable();

private:
    class KV
    {
    public:
        std::string Key;
        std::string Value;
        struct KV *next;
        KV(std::string k, std::string v):Key(k),Value(v), next(NULL) {};
    };
    std::vector<KV*> hashContent;
    int getHashPosition(std::string key);
};
