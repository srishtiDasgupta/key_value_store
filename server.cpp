#include <iostream>
#include <pthread.h>
#include <mutex>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
//#include <map>
#include <thread>
#include <functional>
#include <type_traits>
#include <cstdlib>
#include <cassert>
//#include "HashTable.h"
#include "Cache.h"
#include <climits>

#define MYPORT 32001
#define BACKLOG 10

//using namespace std;

HashTable ht(2);
Cache cache(1,&ht);
//std::map<std::string,std::string> hmp;
std::vector<pthread_t> T;
//std::vector<std::thread> T;
std::string client;
// std::string cmmunicated_pc;
struct sockaddr_in client_address;
std::mutex lock_1;

void* call_from_thread(void *newSocket)
{

    int sock = *(int*)newSocket;

    std::cout << "LOADING PLEASE WAIT -------->>" << std::endl << std::endl;
    char buffer[1024];

    while(1)
    {
        lock_1.try_lock();
        memset (buffer,'\0',1024);
        recv(sock, buffer, 1024, 0);
        std::string choice = buffer;

        std::cout << "Server has recieved the option of: " << buffer << std::endl;

        if(choice.compare("1") == 0)//if(strcmp(buffer,"1"))
        {
            std::cout << "Inserting KEY-VALUE pair" << std::endl;
            memset (buffer,'\0',1024);
            strcpy(buffer,"send");
            send(sock, buffer, strlen(buffer), 0);
            memset (buffer,'\0',1024);
            recv(sock, buffer, 1024, 0);
            std::cout << "Recieved the KEY: " << buffer << std::endl;
            std::string KEY = buffer;
            std::cout << "KEY to be inserted: " << KEY << std::endl;
            memset (buffer,'\0',1024);
            strcpy(buffer,"send1");
            send(sock, buffer, strlen(buffer), 0);
            memset (buffer,'\0',1024);
            recv(sock, buffer, 1024, 0);
            std::cout << "Recieved the VALUE: " << buffer << std::endl;
            std::string VALUE = buffer;
            std::cout << "VALUE to be inserted: " << VALUE << std::endl;
            // hmp.insert(std::pair<std::string,std::string>(KEY,VALUE));

            bool answer = cache.PUT(KEY, VALUE);
            if(answer == true)
                std::cout << "Insertion Successful !!!" << std::endl;
            else
                std::cout << "Insertion failed !!!" << std::endl;

        }
        else if(choice.compare("2") == 0)//else if(strcmp(buffer,"2"))
        {
            std::cout << "Deleting KEY-VALUE pair" << std::endl;
            memset (buffer,'\0',1024);
            strcpy(buffer,"send");
            send(sock, buffer, strlen(buffer), 0);
            memset (buffer,'\0',1024);
            sleep(2);
            recv(sock, buffer, 1024, 0);
            std::cout << "Recieved the KEY: " << buffer << std::endl;
            std::string KEY = buffer;
            std::cout << "KEY to be deleted: " << KEY << std::endl;
            // std::map<std::string,std::string> :: iterator it;
            // it = hmp.find(KEY);
            // if(it != hmp.end())
            // 	hmp.erase(it);
            // else
            // 	continue;

            bool answer = cache.DELETE(KEY);
            if(answer == true)
                std::cout << "Deletion Successful !!!" << std::endl;
            else
                std::cout << "Deletion failed !!!" << std::endl;


        }
        else if(choice.compare("3") == 0)//else if(strcmp(buffer,"3"))
        {
            std::cout << "Retrieving KEY-VALUE pair" << std::endl;
            memset (buffer,'\0',1024);
            strcpy(buffer,"send");
            send(sock, buffer, strlen(buffer), 0);
            memset (buffer,'\0',1024);
            sleep(2);
            recv(sock, buffer, 1024, 0);
            std::cout << "Recieved the KEY: " << buffer << std::endl;
            std::string KEY = buffer;
            std::cout << "KEY to be retrieved: " << KEY << std::endl;
            // std::map<std::string,std::string> :: iterator it;
            // it = hmp.find(KEY);
            // if(it != hmp.end())
            // 	std::cout << "VALUE IS: " << it->second << std::endl;
            // else
            // 	continue;

            std::string answer = cache.GET(KEY);
            if(answer.empty() == true)
                std::cout << "Invalid key given as input" << std::endl;
            else
            {
                std::cout << "VALUE retrieved: " << answer << std::endl;
                memset (buffer,'\0',1024);
                strcpy(buffer, answer.c_str());
                send(sock, buffer, strlen(buffer), 0);
            }
        }
        else if (choice.compare("4") == 0)
        {
            std::cout << "Printing the KEY-VALUE store" << std::endl;
            // std::map<std::string,std::string> :: iterator it;
            // for(it = hmp.begin(); it != hmp.end(); it++)
            // 	std::cout << it->first << "\t" << it->second << std::endl;
            ht.printTable();

        }

        // cout << "Going to sleep" << endl;
        // sleep(5);
        // cout << "Wake sleep" << endl;
        lock_1.unlock();
    }

    return 0;
}

int main(int argc, char *argv[])
{

    std::cout << "I am inside the server ..." << std::endl;

    int sockfd,server_len,bin,n;
    int *new_sock;
    struct sockaddr_in server_address;
    sockfd=socket(AF_INET,SOCK_STREAM,0);
    if(sockfd==-1)
    {
        std::cout << "Sorry! Socket can not be created" << std::endl;
        exit(0);
    }
    server_address.sin_family=AF_INET;
    server_address.sin_port=htons(MYPORT);
    server_address.sin_addr.s_addr=inet_addr("127.0.0.1");   /////192.168.200.100
    server_len=sizeof(server_address);
    bin=bind(sockfd,(struct sockaddr *)&server_address,server_len);
    if(bin==-1)
    {
        std::cout << "Error in binding.." << std::endl;
        exit(0);
    }
    int l=listen(sockfd,BACKLOG);

    if(l==-1)
    {
        std::cout << "Server:Error in listening on socket "<<  std::endl;
        exit(1);
    }

    std::cout << "WAITING FOR CLIENT" << std::endl;
    int newSocket;
    int cz=sizeof(struct sockaddr_in);
    int num1 = 0;
    int i = 0;
    while((newSocket = accept(sockfd,(struct sockaddr*)&client_address,(socklen_t *)&cz)))
    {
        pthread_t threadID;
// 		std::thread threadID;
        std::cout << "SOCKET ID: " << newSocket << "\t" << "IP ADDRESS: " << inet_ntoa(client_address.sin_addr) << std::endl;

        std::cout << "Connection Accepted: " << std::endl;

        T.push_back(threadID);

        if(pthread_create( &threadID , NULL , call_from_thread , (void*) &newSocket)<0)
        {
            std::cerr << "Thread could not be launched" << std::endl;
            return 1;
        }

        //     if( T.push_back(std::thread threadID(call_from_thread, (void*) &newSocket)) ! = 0)
        //     {
        // 	    std::cerr << "Thread could not be launched" << std::endl;
        // 	    return 1;
        // 	}

        //  		std::cout << "Handler assigned" << std::endl;
        //  		i++;
        // }

        // for (auto& th : T) th.join();

    }
}