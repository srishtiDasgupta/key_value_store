#include "Cache.h"
#include <climits>

/*====================================================================
 *                      PUBLIC MEMBER FUNCTIONS
 *====================================================================*/

bool Cache::DELETE(const std::string key)
{
    std::string value = Cache::GET(key);//check if its there in cache else go to table and bring to cache
    if (!value.empty())
    {
        for (int i=0; i < Cache::cacheVec.size(); i++)
        if(Cache::cacheVec[i].key == key)
        {
            if(Cache::cacheVec[i].isDirty)
            {  //if the data inserted into cache, not updated into table
                Cache::cacheVec.erase(Cache::cacheVec.begin()+i);
                return true;
            }
            Cache::cacheVec.erase(Cache::cacheVec.begin()+i);//for data which are loaded from table
            return ht->remove(key);
        }
    }

    return false;
}


bool Cache::PUT(const std::string key, const std::string val)
{
    std::string value = Cache::GET(key);//check if its there in cache else go to table and bring to cache
    if (value.empty())
    {
        if (Cache::cacheVec.size() == Cache::maxCapacity)
        {  //if cache full then clean up
            Cache::cacheEvictionStartegy();
        }
        Cache::cacheVec.push_back({key, val, true, false,0});
        return true;
    }

    return false;
}


std::string Cache::GET(const std::string key)
{
    std::string value =  Cache::getFromCache(key);
    if (value.empty())
    {
        value = Cache::getFromtable(key);
    }
    return value;
}

//constructor
Cache::Cache(int capsity, HashTable *htable) 
{
    maxCapacity=capsity;
    cacheVec.reserve(maxCapacity);
    ht = htable;
}

//destructor
Cache::~Cache() 
{
    Cache::updateHashTable();
}

/*====================================================================
 *                      PRIVATE MEMBER FUNCTIONS
 *====================================================================*/

std::string Cache::getFromCache(const std::string key) 
{
    std::string val;
    for (int i=0; i<Cache::cacheVec.size(); i++)
    {
        if(Cache::cacheVec[i].key==key)
        {
            std::cout << key << " found in cache!" << std::endl;
            Cache::cacheVec[i].usageCount+=1;
            val = Cache::cacheVec[i].value;
        }
    }
    return val;
}

//if element is not in cache, bring it from table
std::string Cache::getFromtable(const std::string key)
{
    std::string val = ht->retrieve(key);
    if (!val.empty())
    {
        if (Cache::cacheVec.size() == Cache::maxCapacity)
        {
            Cache::cacheEvictionStartegy();
        }
        Cache::cacheVec.push_back({key, val, false, false,0});
    }
    return val;
}

//free up cache block strategy
void Cache::cacheEvictionStartegy() 
{
    std::vector<int> toBeDeleted;
    int lruIndex;
    int min = INT_MAX;
    for (int i=0; i<Cache::cacheVec.size(); i++)
    {
        //free up all the element which are deleted
        if(Cache::cacheVec[i].operation==1)
        {
            toBeDeleted.push_back(i);
        }
        if(min>Cache::cacheVec[i].usageCount)
        {
            min=Cache::cacheVec[i].usageCount;
            lruIndex=i;
        }
    }

    //1st free up element which are delted
//    if(toBeDeleted.size()>0){
//        int decreaseIndex = 0;
//        std::cout<<"Evecting ";
//        for(auto i: toBeDeleted){
//            i -=decreaseIndex++;
//            std::cout<<Cache::cacheVec[i].key<<" ";
//            if(Cache::cacheVec[i].isDirty){
//                Cache::deleteFromTable(Cache::cacheVec[i].key);
//            }
//            Cache::cacheVec.erase(Cache::cacheVec.begin()+i);
//        }
//        std::cout<<"from cache as they are deleted"<<std::endl;
//    } else{ //else just evect lru one
        std::cout << "Evecting " << Cache::cacheVec[lruIndex].key << " from cache using lru." << std::endl;
        if(Cache::cacheVec[lruIndex].isDirty)
        {
            if(Cache::cacheVec[lruIndex].operation == 1)
            {
                Cache::deleteFromTable(Cache::cacheVec[lruIndex].key);
            } 
            else
            {
                Cache::insertToHashTable(Cache::cacheVec[lruIndex].key, Cache::cacheVec[lruIndex].value);
            }
        }
        Cache::cacheVec.erase(Cache::cacheVec.begin()+lruIndex);
}

bool Cache::deleteFromTable(const std::string key) 
{
    std::string value = Cache::getFromtable(key);
    if(!value.empty())
    {
        return ht->remove(key);
    }
    return false;
}

bool Cache::insertToHashTable(const std::string key, const std::string value) 
{
    return ht->enter(key, value);
}

bool Cache::updateHashTable() 
{
    for (int i=0; i<Cache::cacheVec.size(); i++)
    {
        if(Cache::cacheVec[i].isDirty)
        {
            //todo remove delete step as it was handled previously
            if(Cache::cacheVec[i].operation==1 && Cache::cacheVec[i].isDirty)
            {
                Cache::deleteFromTable(Cache::cacheVec[i].key);
            }
            else
            {
                Cache::insertToHashTable(Cache::cacheVec[i].key, Cache::cacheVec[i].value);
            }

        }
    }
    return false;
}

