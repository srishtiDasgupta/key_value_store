#ifndef HASHTABLE_CACHE_H
#define HASHTABLE_CACHE_H
#endif

#include <iostream>
#include <vector>
#include <string.h>
#include "HashTable.h"


class Cache {
public:
    bool DELETE(const std::string key);
    bool PUT(const std::string key, const std::string value);
    std::string GET(const std::string key);
    int maxCapacity;
    Cache(int n, HashTable *htable); //max size of cache
    ~Cache();

private:
    struct cacheElem{
        std::string key;
        std::string value;
        bool isDirty;
        //todo use enum in future
        bool operation;// 0 for insert, 1 for delete
        int usageCount;
    };
    HashTable *ht;
    std::vector<cacheElem> cacheVec;
    bool updateHashTable();//call before exiting the program
    void cacheEvictionStartegy();
    std::string getFromCache(const std::string key);
    std::string getFromtable(const std::string key);
    bool deleteFromTable(const std::string key);
    bool insertToHashTable(const std::string key, const std::string value);

};

