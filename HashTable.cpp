#include <vector>
#include <functional>
#include <type_traits>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include "HashTable.h"

/*====================================================================
 *						PUBLIC MEMBER FUNCTIONS
 *====================================================================*/

HashTable::HashTable(int n)//constructor
{
	HashTable::maxBucket = n;
	hashContent.resize(maxBucket,NULL);

}

HashTable::~HashTable() //destructor
{
	hashContent.clear();
}

void HashTable::printTable() 
{
	for(int i = 0;i < maxBucket; i++)
	{
		std::cout << "In Hash bucket : " << i << std::endl;
		KV *ptr = HashTable::hashContent[i];
		while(ptr)
		{
			std::cout << "Key : " << ptr->Key << "  Value : " << ptr->Value << std::endl;
			ptr = ptr->next;
		}

	}
}

bool HashTable::enter(std::string key, std::string value)
{
	int index = HashTable::getHashPosition(key) & (HashTable::hashContent.capacity()-1); // REFER TO gfg

	// //check if rehashing is required
	// bool action = rehashingReq();

	// if(action == true)
	// 	rehashing();

	KV* node = new KV(key, value);


	if( HashTable::hashContent[index] == NULL) // check if the position returned by the getHashPosition() is empty
	{
        HashTable::hashContent[index] = node;
		return true;
	}
	else // in this case where the hash function returns the same index, then add the node to the end of the linked list
	{
		//struct KV *head = &HashTable::hashContent[index];
		KV *ptr = HashTable::hashContent[index];
		if (ptr->Key.compare(key)==0)
		{
			return false;
		}
		while(ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		ptr->next = node;
		return true;
	}
}

std::string HashTable::retrieve(std::string key)
{
	std::string val;
	int index = HashTable::getHashPosition(key) & (HashTable::hashContent.capacity()-1);
	//todo refine this code, can be reduce with return val
	//if there exists no single record in hashContent[index]
	if(HashTable::hashContent[index]==NULL)
		return val;
	else  //iterate over the linked list to find which node contains the KEY
	{
		KV *ptr = HashTable::hashContent[index];
		while(ptr)
		{
			if(ptr->Key.compare(key) == 0)
				return ptr->Value;
			else
				ptr = ptr->next;
		}
	}
	return val;
}
bool HashTable::remove(std::string key)
{
 	int index = HashTable::getHashPosition(key) & (HashTable::hashContent.capacity()-1);

 	if(HashTable::hashContent[index] == NULL) 	//if there exists no single record in hashContent[index]
 		return false;

 	else if ( HashTable::hashContent[index]->Key.compare(key) == 0 ) //the data in the index itself is to be deleted
 	{
 		HashTable:: KV *ptr = HashTable::hashContent[index];
 		HashTable::hashContent[index] = HashTable::hashContent[index]->next;
 		delete ptr;
 		return true;
 	}
 	else					// else traverse the loop and delete desired element
	{
		KV *ptr = HashTable::hashContent[index];
		KV *tmp = ptr->next;

		while(tmp != NULL)
		{
			if(tmp->Key.compare(key)==0 )
			{
				ptr->next = tmp->next;
				delete tmp;
				return true;
			}
			tmp = tmp->next;
			ptr = ptr->next;
		}
		return false;
	}

}
/*====================================================================
 *						PRIVATE MEMBER FUNCTIONS
 *====================================================================*/

int HashTable::getHashPosition(std::string key)
{
	//hash func inspired by Honer Rule approx to prevent overflow
	int hashVal = 0, asc;
	for(int i = 0; i < key.size(); i++)
	{
		asc = key[i] > 96 ? key[i] - 96 : key[i] - 64;
		hashVal = (hashVal * 32 + asc) % hashContent.capacity(); //not hashContent.size() as this value is dynamic.... 
	}
	return hashVal;
}

// bool rehashingReq()
// {
// 	if ( hashContent.size() >= 3 / 4 * hashContent.capacity())
// 		return true;
// 	else
// 		return false;
// }

// void rehashing()
// {

// }



