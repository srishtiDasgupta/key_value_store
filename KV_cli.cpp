#include<stdlib.h>
#include<stdio.h>
#include<string>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<thread>
#include<arpa/inet.h>
#include<iostream>

//using namespace std;


int main(int argc,char *argv[])
{
	int clientSocket,newsockfd,portno;
	struct sockaddr_in serv_addr;
	struct hostnet *server;

	clientSocket=socket(AF_INET,SOCK_STREAM,0);
	if(clientSocket<0)
		std::cout << "Error opening socket" << std::endl;
	bzero((char*)&serv_addr,sizeof(serv_addr));
	portno=atoi(argv[1]);
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=inet_addr("127.0.0.1");
	serv_addr.sin_port=htons(portno);

	if(connect(clientSocket,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		std::cerr << "Error on connecting" << std::endl;
	else
		std::cout << "Successful connection" << std::endl;

    std::cout << "SOCKET: " << clientSocket << std::endl << std::endl;

    std::cout << "You as a client can now access the KEY-VALUE Store of the Server " << std::endl << std::endl;
    int choice;
    int c = 1;

    while(c == 1)
	{	
        std::cout << " Choose any of the three options....[1,2,3,4]" << std::endl << std::endl;
        std::cout << " 1. PUT " << std::endl;
        std::cout << " 2. DELETE " << std::endl;
        std::cout << " 3. GET " << std::endl << std::endl;
        std::cout << " 4. PRINT CONTENTS OF HASH TABLE" << std::endl << std::endl;

        std::cout << " Enter your choice....: ";
        std::cin >> choice;
        char buffer[1024];

        switch(choice)
        {
            case 1:
            {
                memset (buffer,'\0',1024);
                std::cout << "--- INSERTING A KEY VALUE PAIR ---" << std::endl;
                strcpy(buffer,"1");
                send(clientSocket, buffer, strlen(buffer), 0);
                std::string key;
                std::string value;
                memset (buffer,'\0',1024);
                recv(clientSocket, buffer, 1024, 0);
                std::string S = buffer;
                //cout << "Client recieved " << buffer << " from server" << endl;

                if(S.compare("send") == 0)
                {
                    memset (buffer,'\0',1024);
                    std::cout << "Insert the KEY : " << std::endl;
                    std::cin >> buffer;
                    send(clientSocket, buffer, strlen(buffer), 0);
                }

                memset (buffer,'\0',1024);
                recv(clientSocket, buffer, 1024, 0);
                std::cout << "Client recieved " << buffer << " from server" << std::endl;
                S = buffer;

                if(S.compare("send1") == 0)
                {
                    memset (buffer,'\0',1024);
                    std::cout << "Insert the VALUE : " << std::endl;
                    std::cin >> buffer;
                    send(clientSocket, buffer, strlen(buffer), 0);
                }                

                break;
            } 
            case 2: 
            {
                memset (buffer,'\0',1024);
                std::cout << "--- DELETING A KEY VALUE PAIR ---" << std::endl;
                strcpy(buffer,"2");
                send(clientSocket, buffer, strlen(buffer), 0);
                std::string K;
                memset (buffer,'\0',1024);
                recv(clientSocket, buffer, 1024, 0);
                std::string S = buffer;

                if(S.compare("send") == 0)
                {
                    memset (buffer,'\0',1024);
                    std::cout << "Enter the KEY of the record to be deleted" << std::endl;
                    std::cin >> buffer;
                    send(clientSocket, buffer, strlen(buffer), 0);
                }

                break;
            }
            case 3:
            {
                memset (buffer,'\0',1024);
                std::cout << "--- RETRIEVING AN ITEM ---" << std::endl;
                strcpy(buffer,"3");
                send(clientSocket, buffer, strlen(buffer), 0);
                std::string K;
                memset (buffer,'\0',1024);
                recv(clientSocket, buffer, 1024, 0);
                std::string S = buffer;

                if(S.compare("send") == 0)
                {
                    memset (buffer,'\0',1024);                
                    std::cout << "Enter the key of the value to be retrieved" << std::endl;
                    std::cin >> buffer;
                    send(clientSocket, buffer, strlen(buffer), 0);
                }

                memset (buffer,'\0',1024); 
                recv(clientSocket, buffer, 1024, 0);
                std::cout << "Value retrieved:" << buffer << std::endl;

                break;
            }

            case 4:
            {
                memset (buffer,'\0',1024);
                std::cout << "--- PRINTING MAP ---" << std::endl;
                strcpy(buffer,"4");
                send(clientSocket, buffer, strlen(buffer), 0); 
                break;
            }

            default: 
            {
                std::cout << "Incorrect Choice" << std::endl;
                break;
            }
        }


        std::cout << "Press 1 to continue, else press any other key...." << std::endl;

        std::cin >> c;
        if(c == 1)
            continue;
        else
            break;
    }

}
